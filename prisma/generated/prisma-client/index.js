"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Condominium",
    embedded: false
  },
  {
    name: "Cost",
    embedded: false
  },
  {
    name: "MeltedMp",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `https://calm-taiga-93742.herokuapp.com/reaper-dashboard/dev`,
  secret: `truesecret`
});
exports.prisma = new exports.Prisma();
