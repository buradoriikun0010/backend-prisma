const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('../prisma/generated/prisma-client')

const resolvers = {
  Query: {
    condominiums: (_, args, context) => {
      // ...
      return context.prisma.condominiums()
    },
    condominium: (_, {id}, context) => {
        return context.prisma.condominium(
            {
                id
            }
          )
    },
    getAllMps: (_, args, context) => context.prisma.meltedMps(),
    getOneMp: (_, {id}, context) => prisma.meltedMp(
        {
            id
        }
      )
  },
  Condominium: {
    meltedMps: parent => {
      return prisma.condominium({id: parent.id}).meltedMps()
      },
    id: (parent, args, context) => parent.id
    },

 }

const server = new GraphQLServer({
  typeDefs: 'src/schema.graphql',
  resolvers,
  context: { prisma }
})
server.start(() => console.log(`GraphQL server is running on http://localhost:4000`))